<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // Define the response
        $response = [
            'code' => 400,
            "message" => $exception->getMessage(),
            "errors" => [
                "errorMessage" => $exception->getMessage(),
                "errorDetails" => $exception->getTrace()
            ]
        ];
        // Default response of 400
        $code = 400;
        // dd($exception);
        // If the app is in debug mode
        // if (config('app.debug')) {
        //     // Add the exception class name, message and stack trace to response
        //     $response['exception'] = get_class($exception); // Reflection might be better here
        //     $response['message'] = $exception->getMessage();
        //     $response['trace'] = $exception->getTrace();
        // }


        // If this exception is an instance of HttpException
        if ($this->isHttpException($exception)) {
            // Grab the HTTP status code from the Exception
            $response["internalMessage"] = $exception->getMessage() ?: "Not Found";
            $code = $exception->getStatusCode();
        }

        if ($exception instanceof Tymon\JWTAuth\Exceptions\TokenExpiredException) {
            $response["userMessage"] = "Invalid Token";
            $response["internalMessage"] = "token_expired";
        } else if ($exception instanceof Tymon\JWTAuth\Exceptions\TokenInvalidException) {
            $response["userMessage"] = "Invalid Token";
            $response["internalMessage"] = "token_invalid";
        }

        if ($exception instanceof AuthenticationException ) {
            $code = 401;
        }

        // Return a JSON response with the response array and status code
        $response["code"] = $code;
        return response()->json($response, 200);
        // return parent::render($request, $exception);
    }
}
