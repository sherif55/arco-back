<?php

namespace App\Http\Controllers;

use App\Arco\Zones\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ZonesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $zones = Zone::all();

        return $this->jsonResponse("Success", $zones);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "name" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $zone = Zone::create($request->all());

        return $this->jsonResponse("Success", $zone);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $zone = Zone::findOrFail($id);

        return $this->jsonResponse("Success", $zone);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "name" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $zone = Zone::findOrFail($id);
        $zone->name = $request->name;
        $zone->description = $request->description;
        $zone->save();

        return $this->jsonResponse("Success", $zone);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $zone = Zone::findOrFail($id);

        // if($zone->units->count()){
        //     return $this->errorResponse("Cannot Delete Zone with Units", "Cannot Delete Zone with Units", [], 400);
        // }

        $zone->delete();

        return $this->jsonResponse("Success");
    }

    public function bulkDelete(Request $request)
    {
        // get zones
        $zones = Zone::whereIn("id", $request->ids)->get();

        // filter zones
        $filtered = $zones->filter(function ($zone)
        {
            return $zone->units->count() == 0 || $zone->facilities->count() == 0; 
        });

        $exceptions = ($zones->count() != $filtered->count());

        $deleted_ids = $filtered->pluck("id")->toArray();

        Zone::whereIn("id", $deleted_ids)->delete();

        return $this->jsonResponse("Success", [
            "exceptions" => (int)$exceptions,
            "deleted_ids" => $deleted_ids
        ]); 
    }
}
