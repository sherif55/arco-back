<?php

namespace App\Http\Controllers\Api;

use App\Arco\Users\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LinkedAccountsController extends Controller
{
    
    public function index()
    {
    	$user = \Auth::user();

    	$accounts = $user->linked;

    	return $this->jsonResponse("Success", $accounts);
    }

    public function store(Request $request)
    {
    	// validate
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "phone" => "required|unique:users,phone"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $user = User::where("phone", $request->phone)->get();
        if($user->count()){
            return $this->errorResponse("Phone number already exists", "invalid data", $validator->errors(), 422);   
        }

        $user = new User;
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->parent_id = \Auth::id();
        $user->save();

        return $this->jsonResponse("Success", $user);
    }

    public function unlinkAccount($id)
    {
    	$user = \Auth::user();

    	$account = User::findOrFail($id);
        $account->delete();
        
    	// if($user->id != $account->parent_id){
    	// 	return $this->errorResponse("Account does not belong to this user", "invalid data", $validator->errors(), 422);
    	// }

    	// $account->parent_id = null;
    	// $account->save();

    	return $this->jsonResponse("Success", true);
    }
}
