<?php

namespace App\Http\Controllers\Api;

use App\Arco\Users\Notification;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    
    public function index()
    {
    	$notifications = \Auth::user()->notifications()->latest()->get()->groupBy(function ($item)
    	{
    		return $item->created_at->format("Y-m-d");
    	})->toArray();

    	$notificationsArray = [];
    	$ind = 0;
    	foreach ($notifications as $key => $not) {
    		$notificationsArray[$ind]["date"] = $key;
    		$notificationsArray[$ind]["data"] = $not;
    		$ind++;
    	}

    	return $this->jsonResponse("Success", $notificationsArray);
    }
}
