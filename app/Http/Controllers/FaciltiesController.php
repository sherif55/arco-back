<?php

namespace App\Http\Controllers;

use App\Arco\Zones\Facility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FaciltiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facilities = Facility::with("zone", "type")->get();

        return $this->jsonResponse("Success", $facilities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "zone_id" => "required",
            "type_id" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $facility = Facility::create($request->all());

        return $this->jsonResponse("Success", $facility);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $facility = Facility::findOrFail($id);

        return $this->jsonResponse("Success", $facility);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $facility = Facility::findOrFail($id);

        // validate
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "zone_id" => "required",
            "type_id" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse("Invalid Data", "invalid data", $validator->errors(), 422);
        }

        $facility->name = $request->name;
        $facility->description = $request->description;
        $facility->zone_id = $request->zone_id;
        $facility->type_id = $request->type_id;
        $facility->save();

        return $this->jsonResponse("Success", $facility);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facility = Facility::findOrFail($id);

        $facility->delete();

        return $this->jsonResponse("Success");
    }

    public function bulkDelete(Request $request)
    {
        Facility::whereIn("id", $request->ids)->delete();

        return $this->jsonResponse("Success", [
            "exceptions" => 0,
            "deleted_ids" => $request->ids
        ]); 
    }
}
