<?php

namespace App\Arco\Zones;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    
    protected $fillable = ["name", "description", "zone_id", "type_id"];

    public function zone()
    {
    	return $this->belongsTo(Zone::class);
    }

    public function type()
    {
    	return $this->belongsTo(Type::class);
    }
}
