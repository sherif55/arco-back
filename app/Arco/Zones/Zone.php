<?php

namespace App\Arco\Zones;

use App\Arco\Zones\Facility;
use App\Arco\Zones\Unit;
use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    
    protected $fillable = ["name", "description"];

    protected $appends = ["units_count", "facilities_count"];

    public function units()
    {
    	return $this->hasMany(Unit::class);
    }

    public function facilities()
    {
    	return $this->hasMany(Facility::class);
    }

    public function getUnitsCountAttribute()
    {
    	return $this->units->count();
    }

    public function getFacilitiesCountAttribute()
    {
    	return $this->facilities->count();
    }
}
