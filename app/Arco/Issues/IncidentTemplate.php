<?php

namespace App\Arco\Issues;

use Illuminate\Database\Eloquent\Model;

class IncidentTemplate extends Model
{
    
    protected $fillable = ["name", "description"];
}
