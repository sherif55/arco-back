<?php 

namespace App\Arco\Issues;

use App\Arco\Staff\TechnicianTransformer;
use App\Arco\Transformers\Transformer;

class IssuesTransformer extends Transformer {

	private $techTrans;

	public function __construct(TechnicianTransformer $techTrans)
	{
		$this->techTrans = $techTrans;
	}


	public function transform($issue)
	{
		return [
			"id" => $issue->id,
			"description" => $issue->description,
			"state_id" => $issue->state,
			"state" => $issue->issue_state ? $issue->issue_state->name : null,
			"type" => $issue->type,
			"rating" => $issue->rating,
			"done" => $issue->done,
			"reason" => $issue->reason,
			"request_date" => $issue->request_date,
			"request_time" => $issue->request_time,
			"assignee" => $issue->assignee ? $this->techTrans->transform($issue->assignee) : null,
			"extended_issues" => $this->transformCollection($issue->extended_issues)
 		];
	}
}