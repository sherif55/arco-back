<?php

namespace App\Arco\Issues;

use Illuminate\Database\Eloquent\Model;

class IssueTemplate extends Model
{
    
    protected $fillable = ["name", "description"];
}
