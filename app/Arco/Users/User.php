<?php

namespace App\Arco\Users;

use App\Arco\Issues\Issue;
use App\Arco\Staff\TechnicianProfile;
use App\Arco\Users\Country;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\URL;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', "phone", "country_id", "contract_id", "birthdate"
    ];

    protected $appends = ["image", "id_image", "license_image", "country_name"];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', "verification_code", "active"
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function linked()
    {
        return $this->hasMany(self::class, "parent_id");
    }

    public function parent()
    {
        return $this->belongsTo(self::class, "parent_id");
    }

    public function country()
    {
        return $this->belongsTo(Country::class, "country_id");
    }

    public function issues()
    {
        return $this->hasMany(Issue::class, "user_id");
    }

    public function assigned_issues()
    {
        return $this->hasMany(Issue::class, "assignee_id");
    }

    public function tech()
    {
        return $this->hasOne(TechnicianProfile::class, "user_id");
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, "user_id");
    }

    public function getImageAttribute()
    {
        if(isset($this->attributes["image"])){
            return URL::to('') . "/" . $this->attributes["image"];
        }
    }

    public function getIdImageAttribute()
    {
        if(isset($this->attributes["id_image"])){
            return URL::to('') . "/" . $this->attributes["id_image"];
        }
    }

    public function getLicenseImageAttribute()
    {
        if(isset($this->attributes["license_image"])){
            return URL::to('') . "/" . $this->attributes["license_image"];
        }
    }

    public function getCountryNameAttribute()
    {
        if($this->country){
            return $this->country->name;
        }
    }

    public function isLinked()
    {
        return (bool)$this->parent_id;
    }
}
