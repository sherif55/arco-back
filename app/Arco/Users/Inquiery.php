<?php

namespace App\Arco\Users;

use Illuminate\Database\Eloquent\Model;

class Inquiery extends Model
{
    
    protected $fillable = ["name", "description", "email", "phone"];
}
