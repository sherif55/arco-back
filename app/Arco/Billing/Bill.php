<?php

namespace App\Arco\Billing;

use App\Arco\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Bill extends Model
{
    
	protected $fillable = ["name", "resident_id", "amount", "service_id", "bill_date", "max_date", "description", "status"];

    protected $appends = ["image"];

    public function service()
    {
    	return $this->belongsTo(Service::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class, "resident_id");
    }

    public function getImageAttribute()
    {
    	if(isset($this->attributes["image"])){
            return URL::to('') . "/" . $this->attributes["image"];
        }
    }
}
