<?php

namespace App\Arco\Billing;

use App\Arco\Billing\Bill;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    
    protected $fillable = ["name", "description"];

    public function bills()
    {
    	return $this->hasMany(Bill::class, "service_id");
    }
}
