<?php

namespace App\Arco\Services;

use App\Arco\ACL\Role;
use App\Arco\Users\User;

/**
* 
*/
class AccountService
{
	
	function createAccount($first_name, $last_name, $phone_number, $country_code, $email = null, $password = null)
	{
		$user = User::create([
			"first_name" => $first_name,
			"last_name" => $last_name,
			"phone" => $phone_number,
			"email" => $email
		]);

		if($password){
			$user->password = bcrypt($password);
			$user->save();
		}

		return $user;
	}

	public function updateAccount(User $user, $first_name, $last_name, $phone_number, $country_code, $email = null)
	{
		$user->first_name = $first_name;
		$user->last_name = $last_name;
		$user->phone_number = $phone_number;
		$user->country_code = $country_code;
		$user->email = $email;
		$user->save();

		return $user;
	}

	public function addRole(User $user, $role)
	{
		$role = Role::where("name", $role)->get()->first();

		$this->clearRoles($user);
		
		return $user->attachRole($role);
	}

	public function clearRoles(User $user)
	{
		$user->roles()->sync([]);
	}

	public function getAccountByPhone($country_code, $phone_number)
	{
		$user = User::where("country_code", $country_code)->where("phone_number", $phone_number)->get()->first();
		return $user;
	}

	public function validateCode(User $user, $code)
	{
		if($user->verification_code == $code){
			$user->confirmed = 1;
			$user->verification_code = null;
			$user->save();
			return true;
		}

		return false;
	}

	public function createVerificationCode($user)
	{
		$verification_code = strtolower(str_random(4));
		$verification_code = 1234; // temporary for testing
		$user->verification_code = $verification_code;
		$user->save();

		return $user;
	}

	public function deleteAccount(User $user)
	{
		if($user->hasRole("super_admin")){
			throw new \Exception("Cannot delete super admin user", 1);
		}
		$user->delete();
	}

	public function deactivateAccount(User $user)
	{
		$user->active = 0;
		$user->save();

		return $user;
	}

	public function activateAccount(User $user)
	{
		$user->active = 1;
		$user->save();

		return $user;
	}

	public function bulkActivate(array $admin_ids)
	{
		return User::whereIn("id", $admin_ids)->update(["active" => 1]);
	}

	public function bulkDeactivate(array $admin_ids)
	{
		return User::whereIn("id", $admin_ids)->update(["active" => 0]);
	}

	public function bulkDelete(array $admin_ids)
	{
		return User::whereIn("id", $admin_ids)->delete();
	}
}